<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class GreetingsController
{
    public function HelloWorld(): Response
    {
        $data = json_encode([
            ['hi']
        ]);

        return new Response($data, 200);
    }
}
