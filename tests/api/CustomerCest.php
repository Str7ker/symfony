<?php

use \Codeception\Util\HttpCode;

//создание, проверка, обновление, удаление пользователя
class CustomerCest
{
    public function _before(ApiTester $I)
    {
    }

    // создание пользователя
    public function createCustomer(ApiTester $I)
    {
        $data = [
            'name' => 'Ivan',
            'firstname' => 'Butrim',
            'lastname' => 'Andreevich',
            'phone' => '1234',
            'email' => 'email'
        ];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/customer', $data);
        $I->seeResponseCodeIs(HttpCode::CREATED); // 200
        $I->seeResponseIsJson();

        $user_id = $I->grabResponse();
        var_dump($user_id);
        $I->seeResponseContains($user_id);
    }

    //получение пользователя по ID
    public function getCustomer(ApiTester $I)
    {
        $I->sendGet('/api/customer/5ce5f569-2080-11ec-86fa-0a002700000b');
        $I->seeResponseCodeIs(HttpCode::OK); // 200

        $user_id = $I->grabResponse();
        var_dump($user_id);
        $I->seeResponseContains($user_id);
    }

    //обновление данных пользователя по ID
    public function putCustomer(ApiTester $I)
    {
        $data = [
            'name' => 'newIvan',
            'firstname' => 'newButrim',
            'lastname' => 'newAndreevich',
            'phone' => 'new1234',
            'email' => 'newemail'
        ];

        $I->sendPut('/api/customer/5ce5f569-2080-11ec-86fa-0a002700000b', $data);
        $I->seeResponseCodeIs(HttpCode::OK); // 200

        $put_data = $I->grabResponse();
        var_dump($put_data);
        $I->seeResponseContains($put_data);
    }

    //удаление пользователя по ID
    public function deleteCustomer(ApiTester $I)
    {
        $I->sendDelete('/api/customer/5ce5f569-2080-11ec-86fa-0a002700000b');
        $I->seeResponseCodeIs(HttpCode::OK); // 200

        $del_data = $I->grabResponse();
        var_dump($del_data);
        $I->seeResponseContains($del_data);
    }
}

//вывод всех пользователей
class AllCustomersCest
{
    public function _before(ApiTester $I)
    {
    }

    public function getAllCustomers(ApiTester $I)
    {
        $I->sendGet('/api/customers');
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();

        $customers = $I->grabResponse();
        var_dump($customers);
        $I->seeResponseContains($customers);
    }

}

